//
//  IntegViewController.m
//  DevTools
//
//  Created by Balaganesh on 04/04/22.
//  Copyright © 2022 Juspay. All rights reserved.
//

#import "ViewController.h"

// Importing Hyper SDK
// block:start:import-hyper-sdk
#import <HyperSDK/HyperSDK.h>
// block:end:import-hyper-sdk

@interface ViewController ()

// Declaring HyperServices property
@property (nonatomic, strong) HyperServices *hyperInstance;
@property (nonatomic, copy) HyperSDKCallback hyperCallbackHandler;

@end

@implementation ViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        // Creating an object of HyperServices class
        // block:start:create-hyper-services-instance
       
        // block:end:create-hyper-services-instance
    }
    return self;
}

// Creating initiate payload JSON object
// block:start:create-initiate-payload
- (NSDictionary *)createInitiatePayload {
    NSDictionary *innerPayload = @{
        @"action": @"initiate",
        @"merchantId": @"picasso",
        @"clientId": @"picasso",
        @"environment": @"prod"
    };

    NSDictionary *sdkPayload = @{
        @"requestId": NSUUID.UUID.UUIDString,
        @"service": @"in.juspay.hyperpay",
        @"payload": innerPayload
    };

    return sdkPayload;
}
// block:end:create-initiate-payload


// Creating process payload JSON object
// block:start:fetch-process-payload
- (NSDictionary *)createProcessPayload {
    NSDictionary *innerPayload = @{
     // Make an API Call to your server to create Session and return SDK Payload
        @"action": @"paymentPage",
        @"clientId": @"picasso",
        @"merchantId": @"picasso",
        @"environment": @"sandbox",
        @"customerId": @"9742144874",
        @"orderId": @"DW-qIkhzSJ3rZ",
        @"amount": @"100.00",
        @"customerMobile": @"9742144874",
        @"customerEmail": @"test007@gmail.com",
        @"merchantKeyId": @"4794",
        @"orderDetails": @"{\"order_id\":\"DW-qIkhzSJ3rZ\",\"first_name\":\"Test\",\"last_name\":\"Customer\",\"customer_phone\":\"9742144874\",\"customer_email\":\"test@gmail.com\",\"merchant_id\":\"picasso\",\"amount\":\"100.00\",\"customer_id\":\"9742144874\",\"timestamp\":\"1648533750748\",\"return_url\":\"https://sandbox.juspay.in/end\",\"currency\":\"INR\",\"mandate.start_date\":\"1648533750748\",\"mandate.end_date\":\"2134731745451\"}",
        @"signature": @"Vgggk5rFjjimcnabH0t/wWlenMrr/+XI9ax+ldxnU3zVLLLPWKiOinEaxzwBa34veAslb/Y6hYbi+vh6ZYwvESSDVNGs0Y5NOInDpZw3udnYywO5UsPrpMD7xPqYLbNszdrBbAggupojjVP8aM6FeB1gUV5C8D/JM/p7FdZfSblW+fQt1v5zLpeRwFUVVk/NdFzVF6jdzjy6BiE2pLffvbI6osPQlp2jwYkxgyveZPeoUOZ3eZxQxozScDoWo/0ZZGKEWpFjKAZRqRzaQ77dVJQzZk082hf8RHc1rwrCbPXBtb3VjwwQn1bN9h0x43f1G8k8zMrvPSbEwNvjk/x58A=="
    };

    NSDictionary *sdkPayload = @{
        @"requestId": NSUUID.UUID.UUIDString,
        @"service": @"in.juspay.hyperpay",
        @"payload": innerPayload
    };

    return sdkPayload;
}
// block:end:create-process-payload

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.hyperInstance = [[HyperServices alloc] init];
    
    self.hyperCallbackHandler = ^(NSDictionary<NSString *,id> * _Nullable response) {
        NSDictionary *data = response;
        NSString *event = data[@"event"];
        
        if ([event isEqualToString:@"hide_loader"]) {
            // hide loader
        }
        // Handle Process Result
        // This case will reach once payment page closes
        // block:start:handle-process-result

        else if ([event isEqualToString:@"process_result"]) {
            BOOL error = [data[@"error"] boolValue];

            NSDictionary *innerPayload = data[@"payload"];
            NSString *status = innerPayload[@"status"];
            NSString *pi = innerPayload[@"paymentInstrument"];
            NSString *pig = innerPayload[@"paymentInstrumentGroup"];

            if (!error) {
                // txn success, status should be "charged"
                // process data -- show pi and pig in UI maybe also?
                // example -- pi: "PAYTM", pig: "WALLET"
                // call orderStatus once to verify (false positives)
            } else {

                NSString *errorCode = data[@"errorCode"];
                NSString *errorMessage = data[@"errorMessage"];
                if([status isEqualToString:@"backpressed"]) {
                    // user back-pressed from PP without initiating any txn
                }
                else if ([status isEqualToString:@"backpressed"]) {
                    // user initiated a txn and pressed back
                    // poll order status
                } else if ([status isEqualToString:@"pending_vbv"] || [status isEqualToString:@"authorizing"]) {
                    // txn in pending state
                    // poll order status until backend says fail or success
                } else if ([status isEqualToString:@"authorization_failed"] || [status isEqualToString:@"authentication_failed"] || [status isEqualToString:@"api_failure"]) {
                    // txn failed
                    // poll orderStatus to verify (false negatives)
                } else if([status isEqualToString:@"new"]) {
                    // order created but txn failed
                    // very rare for V2 (signature based)
                    // also failure
                    // poll order status
                } else {
                    // unknown status, this is also failure
                    // poll order status
                }
            }
        }
        // block:end:handle-process-result
    };
    
}


- (IBAction)initiatePayments:(id)sender {
    // Calling initiate on hyperService instance to boot up payment engine.
    // block:start:initiate-sdk
    
    NSDictionary *initPayload = [self createInitiatePayload];
    [self.hyperInstance initiate:self payload:initPayload callback:self.hyperCallbackHandler];
}
- (IBAction)startPayments:(id)sender {
    // Calling process on hyperService to open payment page
    // block:start:process-sdk
    NSDictionary *processPayload = [self createProcessPayload];
    [self.hyperInstance process:processPayload];
}


@end
